

name := "spark-edureka"

organization := "com.balab.edu"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.5"


lazy val sparkedureka = (project in file(".")).enablePlugins(JavaAppPackaging)


libraryDependencies ++= {
  val sparkV = "1.6.1"
  Seq(
    "org.apache.spark" % "spark-core_2.10" % sparkV % "provided"
  )
}

mainClass in (Compile) := Some("com.balab.edu.MainApp")
