package com.balab.edu.cdr

import org.apache.spark.{SparkConf, SparkContext}


/**
  * Created by dev-env on 1/6/16.
  */
class CallDetailsRecord {


  def cdr() = {


    val ctx = new SparkContext(new SparkConf().setAppName("CallDetailsRecord"))

    val text =ctx.textFile("CDR.csv", 4)


    val calls  = text.map(x => x.split(",")).map( p => Call( visitor_locn = p(0), call_duration = p(1).toInt, phone_no = p(2), error_code = p(3)) )

    //val collected = calls.collect()

    //collected.foreach( e => println(e.visitor_locn))

    val vob = calls.map( x => (x.visitor_locn,1 )).reduceByKey((x,y) => x + y  ).sortBy(e => e._2, ascending=false)

    val collected = vob.collect()

    val freqCallDrop = collected.take(10) // picks out top ten frequently dropped calls.

    freqCallDrop.foreach(println)
  }
}
